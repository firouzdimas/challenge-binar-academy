const { Pool } = require('pg');
// require('dotenv').config();

class myDB{
    #email = 'admin@mail.com';
    #password = 'admin1234';
    #dbName = 'cafebejs1';
    #config = [
        {
            user: 'postgres',
            host: 'localhost',
            password: 'L3chen4ulti@',
            port: 5432
        },
        {
            user: 'postgres',
            host: 'localhost',
            password: 'L3chen4ulti@',
            database: this.#dbName,
            port: 5432
        }
    ];

    constructor(credentials){
        this.pool = new Pool(this.#config[0]);
        this.authentication = this.#authenticate(credentials);
    };

    #authenticate(credentials){
        return (this.#email === credentials.email && this.#password === credentials.password)
    };

    initDB(){
        if(this.authentication){
            const createDBQuery = `CREATE DATABASE ${this.#dbName};`;
            this.pool.connect()
            .then(async client => {
                try{
                    const res = await client.query(createDBQuery);
                    client.release();
                    await this.pool.end();
                    console.log('Berhasil membuat database!');
                    return this.#createTable();
                }catch(err){ 
                    client.release();
                    await this.pool.end();
                    return console.log('Gagal membuat database!', err);
                };
            });
        }else{
            return console.log('Anda tidak memiliki hak!');
        };
    };

    #createTable(){
        const createTableQuery = 
        `CREATE TABLE crews (
            crew_id serial NOT NULL,
            crew_name varchar(50) NOT NULL,
            no_telp int,
            PRIMARY KEY (crew_id)
        );
        CREATE TABLE positions (
            position_id serial NOT NULL,
            position_name varchar(50) NOT NULL,
            PRIMARY KEY (position_id),
            CONSTRAINT fk_position_id FOREIGN KEY (position_id) REFERENCES crews (crew_id)
        );
        CREATE TABLE products (
            product_id serial NOT NULL,
            product_name varchar(50) NOT NULL,
            PRIMARY KEY (product_id)
        );
        `;
        this.pool = new Pool(this.#config[1]);
        this.pool.connect()
        .then(async client => {
            try{
                const res = await client.query(createTableQuery);
                client.release();
                this.pool.end();
                return console.log('Berhasil membuat tabel!');
            }catch(err){ 
                client.release();
                this.pool.end();
                return console.log('Gagal membuat tabel!', err);
            };
        });
    };

    //CRUD
    async insert(){
        if(this.authentication){
            const insertQuery = [
                `INSERT INTO crews (crew_id, crew_name, no_telp) 
                VALUES 
                (1, 'Zul', 628123), 
                (2, 'ZulFahri', 628123), 
                (3, 'ZulFahriBaihaqi', 628123);`, //index 0, Table 1
                `INSERT INTO positions (position_id, position_name) 
                VALUES 
                (1, 'Barista'), 
                (2, 'Sultan'), 
                (3, 'KangNumpang');`, //index 1, Table 2
                `INSERT INTO products (product_id, product_name) 
                VALUES 
                (1, 'Produk1'), 
                (2, 'Produk2'), 
                (3, 'Produk3');` //index 2, Table 3
            ];
            this.pool = new Pool(this.#config[1]);
            const client = await this.pool.connect();
            return client.query(insertQuery[1], (err, res) => { //jangan lupa pilih table yang sesuai.
                if(err){
                    client.release();
                    this.pool.end();
                    return console.log('Gagal menambahkan data ke tabel!', err);
                };
                client.release();
                this.pool.end();
                return console.log('Berhasil menambahkan data ke tabel!');
            });
        }else{
            return console.log('Anda tidak memiliki hak!');
        };
    };

    async select(){
        if(this.authentication){
            const selectQuery = [
                `SELECT * FROM crews ORDER BY crew_id ASC;`, //index 0
                `SELECT * FROM positions INNER JOIN crews ON positions.position_id = crews.crew_id ORDER BY position_id ASC;`, //index 1
                `SELECT * FROM products ORDER BY product_id ASC` //index 2
            ];
            this.pool = new Pool(this.#config[1]);
            const client = await this.pool.connect()
            return client.query(selectQuery[1], (err, res) => {
                if(err){
                    client.release();
                    this.pool.end();
                    return console.log('Gagal menampilkan tabel!', err);
                };
                client.release();
                this.pool.end();
                return console.log(res.rows);
            });
        }else{
            return console.log('Anda tidak memiliki hak!');
        };
    };

    async update(){
        if(this.authentication){
            const updateQuery = [
                `UPDATE crews SET crew_name = 'Larry' WHERE crew_id = 3;`,
                `UPDATE positions SET position_name = 'KangParkir' WHERE position_id = 3;`,
                `UPDATE product SET product_name = 'Seperit' WHERE product_id = 1;`
        ];
            this.pool = new Pool(this.#config[1]);
            const client = await this.pool.connect()
            return client.query(updateQuery[1], (err, res) => {
                if(err){
                    client.release();
                    this.pool.end();
                    return console.log('Gagal mengubah data!', err);
                };
                client.release();
                this.pool.end();
                return console.log('Berhasil mengubah data!');
            });
        }else{
            return console.log('Anda tidak memiliki hak!');
        };
    };

    async delete(){
        if(this.authentication){
            const deleteQuery = [
                `DELETE FROM crews *;`, //index 0
                `DELETE FROM positions *;`, //index 1
                `DELETE FROM products *;` //index 2
            ];
            this.pool = new Pool(this.#config[1]);
            const client = await this.pool.connect()
            return client.query(deleteQuery[1], (err, res) => {
                if(err){
                    client.release();
                    this.pool.end();
                    return console.log('Gagal menghapus data!', err);
                };
                client.release();
                this.pool.end();
                return console.log('Berhasil menghapus data!');
            });
        }else{
            return console.log('Anda tidak memiliki hak!');
        };
    };
};

const admin1 = new myDB({email: 'admin@mail.com', password: 'admin1234'});

/**
 * Jalankan di awal dan hanya sekali.
 * Akan otomatis membuat database baru beserta 3 table.
 * Nama database dapat diubah pada line 7.
 * Query pembuatan table dapat diubah di dalam method #createTable (line 55).
 * Sorry kak kalau spaghetti code, hehe.
 */

admin1.initDB();

/**
 * Bagian CRUD
 * Hanya bisa dijalankan terpisah satu per satu.
 * Berikan sedikit jeda untuk setiap method.
 */

// admin1.insert();
// admin1.select();
// admin1.update();
// admin1.delete();
