const {Client} = require('pg');
require('dotenv').config();

class database{
    email = "john@mail.com";
    password = "12345";

    constructor(email,password){
        this.authenticated = this.authentication(email,password)
        this.client = new Client({
            user : 'postgres',
            host: 'localhost',
            password: '1q2w3e4r5t',
            port : 5432,
        })
      }
    authentication(email,password){
        return this.email === email && this.password === password
    }

    async createDatabase(namadatabase){
        this.namadatabase = databaseName
        if(this.authenticated){
            const query = `CREATE DATABASE ${namadatabase}`;
        
            this.client.connect();
    
            try{
                const res = await this.client.query(query);
                if(res) console.log('Database Terkoneksi');
            }catch(err){
                console.log('Database Gagal Terkoneksi ');
            }finally{
                this.client.end()
            }
            return true;
        }
        console.log('Anda tidak punya akses');
    }

    createTable = async () => {
        const queryCreateTable = `CREATE TABLE "customers" (
            customer_id int NOT NULL,
            name char (50) NOT NULL,
            age INT NOT NULL,
            city char (50) NOT NULL,
            PRIMARY KEY (customer_id)
          );
          
          CREATE TABLE "accounts"(
            account_id int NOT NULL,
	        balance int NOT NULL,
	        customer_id int NOT NULL,
	        PRIMARY KEY (account_id),
	        FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
);

          
          CREATE TABLE "order"(
            order_id SERIAL NOT NULL,
            order_name VARCHAR(50) NOT NULL,
            price INT NOT NULL,
            customer_id INT NULL REFERENCES customers(customer_id),
            account_id INT NULL REFERENCES accounts(account_id)
          );`;

          if (this.authenticated) {      
            try {
              await this.client.connect();
              await this.client.query(queryCreateTable); 
              return console.log("Tabel Berhasil Dibuat");
            } catch (error) {
              console.error(error.stack);
              return false;
            } finally {
              await this.client.end(); 
            }
          }

          return console.log("Anda tidak memiliki akses");
        };
      
        insertValues = async () => {
            const queryInsert = `INSERT INTO customers(name,age,city) VALUES('Sadewo',22,'Jakarta');
            INSERT INTO accounts(account_id, balance) VALUES(1,2);
            INSERT INTO order(order_name,price,customer_id,account_id) VALUES('PaketHemat',30000,(select customer_id from customers where customer_id = 1),(select account_id from accounts where account_id = 1));`;
        
            if (this.authenticated) {
              // gets connection
              try {
                // gets connection
                await this.client.connect();
                await this.client.query(queryInsert); // sends queries
                return console.log("Insert Value Suceed");
              } catch (error) {
                console.error(error.stack);
                return false;
              } finally {
                await this.client.end(); // closes connection
              }
            }
          };
          readValues = async () => {
            const queryRead = `SELECT customers.customer_id,
                               name,
                               age,
                               city,
                               order_name 
                               FROM 
                                customers 
                               INNER JOIN order 
                                ON order.customer_id = customers.customer_id;`;
              if (this.authenticated) {
                                    
               try {
                                     
                 await this.client.connect();
                                      let result = await this.client.query(queryRead); // sends queries
                                      return console.log(result.rows);
                                    } catch (error) {
                                      console.error(error.stack);
                                      return false;
                                    } finally {
                                      await this.client.end(); // closes connection
                                    }
                                  }
                                };
                              
                                updateValues = async (
                                  table_name,
                                  table_attribute,
                                  attribute_id,
                                  value1,
                                  value2
                                ) => {
                                  const queryUpdate = `UPDATE ${table_name} SET ${table_attribute} = $1 WHERE ${attribute_id} = $2;`;
                              
                                  if (this.authenticated) {
                                    // gets connection
                                    try {
                                      // gets connection
                                      await this.client.connect();
                                      await this.client.query(queryUpdate, [value1, value2]); // sends queries
                                      return console.log("Berhasil Mengubah Value");
                                    } catch (error) {
                                      console.error(error.stack);
                                      return false;
                                    } finally {
                                      await this.client.end(); // closes connection
                                    }
                                  }
                                };
                              
                                deleteValues = async (table_name, table_id, value1) => {
                                  const queryDelete = `DELETE FROM ${table_name} WHERE ${table_id} = $1;`;
                              
                                  if (this.authenticated) {
                                    // gets connection
                                    try {
                                      // gets connection
                                      await this.client.connect();
                                      await this.client.query(queryDelete, [value1]); // sends queries
                                      return console.log("Delete Value Suceed");
                                    } catch (error) {
                                      console.error(error.stack);
                                      return false;
                                    } finally {
                                      await this.client.end(); // closes connection
                                    }
                                  }
                                };
                              }
                              
 const adminPertama = new Admin({ email: "john@mail.com", password: "12345" });

 adminPertama.createDatabase("accounts");

