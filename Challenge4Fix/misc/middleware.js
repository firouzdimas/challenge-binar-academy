require('dotenv').config();
const express = require('express');
const app = express();
const morgan = require('morgan');

const logger = morgan('dev');

//Autentikasi Sederhana Statis aka A**
const authentication = async (req, res, next) => {
    try {
        const userId = 'admin';
        const userPass = 'admin1234';
        if (userId !== process.env.USER_ID || userPass !== process.env.USER_PASS){
          throw new Error('Anda tidak memiliki hak akses!');
        };
        next();
    }catch(error){
        res.status(401).json({
            status: 'error',
            error: error.message
        });
    };
};

module.exports = {
    logger,
    authentication
};
