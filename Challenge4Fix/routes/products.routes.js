const express = require('express');
const router = express.Router();
const { getAllproducts, getproductById, createproduct, updateproduct, deleteproduct } = require('../controllers/products.controller');

router.get('/', getAllproducts);
router.get('/:id', getproductById);
router.post('/', createproduct);
router.patch('/:id', updateproduct);
router.delete('/:id', deleteproduct);

module.exports = router;