const express = require('express');
const router = express.Router();
const { logger, authentication } = require('../misc/middleware');
const customerRoutes = require('./customers.routes');
const orderRoutes = require('./orders.routes')
const productRoutes = require('./products.routes');

router.use(authentication);
router.use(logger);
router.use('/customers', customerRoutes);
router.use('/orders', orderRoutes);
router.use('/products', productRoutes);

module.exports = router;