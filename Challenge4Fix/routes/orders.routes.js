const express = require('express');
const router = express.Router();
const { getAllorders, getorderById, createorder, updateorder, deleteorder } = require('../controllers/orders.controller');

router.get('/', getAllorders);
router.get('/:id', getorderById);
router.post('/', createorder);
router.patch('/:id', updateorder);
router.delete('/:id', deleteorder);

module.exports = router;