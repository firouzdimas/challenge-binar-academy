const { order, customer } = require('../db/models');

const getAllorders = async(req, res) => {
    try{
        let {page, row} = req.query;
        page -= 1;

        const options = ['id', 'food', 'price', 'customerId'];

        if(page) options.offset = page;
        if(row) options.limit = row;

        const allorders = await order.findAll({
            attributes: options,
            include: [{
                model: customer,
                attributes: ['customerId']
            }]
        });

        return res.status(200).json({
            status: 'Success',
            data: allorders
        });
    }catch(error){
        return res.status(500).json({
            status: 'error',
            message: error.message});
    };
};

const getorderById = async(req, res) => {
    try{
        const options = ['id', 'food', 'price', 'customerId'];

        const found = await order.findByPk(req.params.id, {
            attributes: options,
            include: [{
                model: customer,
                attributes: ['customerId']
            }]
        });

        if(!found) throw new Error(`order dengan id ${req.params.id} tidak ditemukan.`);
        return res.status(200).json({
            status: 'Success',
            data: found
        });
    }catch(error){
        return res.status(500).json({
            status: 'error',
            message: error.message});
    };
};

const createorder = async(req, res) => {
    try{
        const {quantity, order_date, customerId} = req.body;
        const created = await order.create({
            quantity: quantity,
            order_date: order_date,
            customerId: customerId
        });
        return res.status(200).json({
            status: 'Berhasil ',
            data: created
        });
    }catch(error){
        return res.status(500).json({
            status: 'error',
            message: error.message});
    };
};

const updateorder = async(req, res) => {
    try{
        const options = ['id', 'food', 'price', 'customerId'];
        const {quantity, order_date, customerId} = req.body;
        const updated = await order.update({
            quantity: quantity,
            order_date: order_date,
            customerId: customerId
        }, {
            where: {
                id: req.params.id
            }
        });
        if(updated){
            const updatedOrder = await order.findByPk(req.params.id, {
                attributes: options,
                include: [{
                    model: customer,
                    attributes: ['cust_name']
                }]
            });
            if(updatedOrder){
                return res.status(200).json({
                    status: `Berhasil mengupdate data dengan id ${req.params.id}`,
                    data: updatedCust
                });
            };
            throw new Error(`order dengan id ${req.params.id} tidak ditemukan.`);
        };
    }catch(error){
        return res.status(500).json({
            status: 'error',
            message: error.message});
    };
};

const deleteorder = async (req, res) => {
    try{
        const deleted = await order.destroy({
            where: {id: req.params.id}
        });
        if(deleted) return res.status(200).json({
            status: `order dengan id ${req.params.id} berhasil dihapus.`,
            data: deleted
        });
        throw new Error(`order dengan id ${req.params.id} tidak ditemukan.`);
    }catch(error){
        return res.status(500).json({
            status: 'error',
            message: error.message});
    };
};

module.exports = {
    getAllorders,
    getorderById,
    createorder,
    updateorder,
    deleteorder
};