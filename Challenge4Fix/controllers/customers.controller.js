const { customer } = require("../models/customer");

const getAllCustomer = async (req, res) => {
    try {
      let { page, row } = req.query;
      page -= 1;
        
      const options = {
            attributes: ['id', 'name', 'age', 'phone_number'],
            offset: page,
            limit: row,
       };
        
        const allCustomer = await Customer.findAll(options);

        res.status(200).json({
            status: "success",
            result: allCustomer,
        });
      } catch (error) {
          console.log(error);
      }
};
const getCustomerById = async (req, res) => {
    const CustomerFound = await Customer.findByPk(req.params.id);
  
    if (!CustomerFound)
      throw new Error(`Game dengan id ${req.params.id} tidak ditemukan`);
    res.status(200).json({
      status: "Success",
      data: CustomerFound,
    });
};
  
const createCustomer = async (req, res) => {
    try {
      const {name, age, phone_number } = req.body;
  
      const createdCustomer = await Customer.create({
        name: name,
        age: age,
        phone_number: phone_number,
      });
      res.status(201).json({
        status: "Success",
        data: createdCustomer,
      });
    } catch (error) {
      console.log(error);
    }
  };
  
  const updateCustomerById = async (req, res) => {
    const customerId = req.params.id;
    try {
      const { name, age, phone_number } = req.body;
      const updateCustomer = await Customer.update(
        { name: name, age: age, phone_number: phone_number },
        {
          where: {
            id: customerId,
          },
        }
      );
      res.status(201).json({
        status: "Success",
        data: req.body,
      });
    } catch (err) {
      console.log(err);
    }
  };
  
  const deleteCustomerById = async (req, res) => {
    const customerId = req.params.id;
    try {
      const deleteCustomer = await Customer.destroy({
        where: {
          id: customerId,
        },
      });
      res.status(201).json({
        status: "Success",
        msg: "Data berhasil di delete",
      });
    } catch (err) {
      console.log(err);
    }
  };
  
  module.exports = {
    getAllCustomer,
    getCustomerById,
    createCustomer,
    updateCustomerById,
    deleteCustomerById,
  };
    