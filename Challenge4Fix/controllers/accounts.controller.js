const { product, order } = require('../db/models');

const getAllproducts = async(req, res) => {
    try{
        let {page, row} = req.query;
        page -= 1;

        const options = ['id', 'balance', 'accountId'];

        if(page) options.offset = page;
        if(row) options.limit = row;

        const allproducts = await product.findAll({
            attributes: options,
            include: [{
                model: order,
                attributes: ['order_date']
            }]
        });

        return res.status(200).json({
            status: 'Success',
            data: allproducts
        });
    }catch(error){
        return res.status(500).json({
            status: 'error',
            message: error.message});
    };
};

const getproductById = async(req, res) => {
    try{
        const options = ['id', 'product_name', 'price', 'orderId'];

        const found = await product.findByPk(req.params.id, {
            attributes: options,
            include: [{
                model: order,
                attributes: ['Product_name']
            }]
        });

        if(!found) throw new Error(`product dengan id ${req.params.id} tidak ditemukan.`);
        return res.status(200).json({
            status: 'Success',
            data: found
        });
    }catch(error){
        return res.status(500).json({
            status: 'error',
            message: error.message});
    };
};

const createproduct = async(req, res) => {
    try{
        const {product_name, price, orderId} = req.body;
        const created = await product.create({
            product_name: product_name,
            price: price,
            orderId: orderId
        });
        return res.status(200).json({
            status: 'Berhasil membuat data.',
            data: created
        });
    }catch(error){
        return res.status(500).json({
            status: 'error',
            message: error.message});
    };
};

const updateproduct = async(req, res) => {
    try{
        const options = ['id', 'product_name', 'price', 'orderId'];
        const {product_name, price, orderId} = req.body;
        const updated = await product.update({
            product_name: product_name,
            price: price,
            orderId: orderId
        }, {
            where: {
                id: req.params.id
            }
        });
        if(updated){
            const updatedProduct = await product.findByPk(req.params.id, {
                attributes: options,
                include: [{
                    model: order,
                    attributes: ['order_date']
                }]
            });
            if(updatedProduct){
                return res.status(200).json({
                    status: `Berhasil mengupdate data dengan id ${req.params.id}`,
                    data: updatedProduct
                });
            };
            throw new Error(`product dengan id ${req.params.id} tidak ditemukan.`);
        };
    }catch(error){
        return res.status(500).json({
            status: 'error',
            message: error.message});
    };
};

const deleteproduct = async (req, res) => {
    try{
        const deleted = await product.destroy({
            where: {id: req.params.id}
        });
        if(deleted) return res.status(200).json({
            status: `product dengan id ${req.params.id} berhasil dihapus.`,
            data: deleted
        });
        throw new Error(`product dengan id ${req.params.id} tidak ditemukan.`);
    }catch(error){
        return res.status(500).json({
            status: 'error',
            message: error.message});
    };
};

module.exports = {
    getAllproducts,
    getproductById,
    createproduct,
    updateproduct,
    deleteproduct
};