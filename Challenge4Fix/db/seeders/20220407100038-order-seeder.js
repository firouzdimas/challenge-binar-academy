'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('orders', [{
      quantity: 5,
      order_date: new Date(),
      createdAt: new Date(),
      updatedAt: new Date(),
      customerId: 1
    }], {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('orders', [{
      quantity: 5,
      order_date: new Date(),
      createdAt: new Date(),
      updatedAt: new Date(),
      customerId: 1
    }], {});
  }
};
