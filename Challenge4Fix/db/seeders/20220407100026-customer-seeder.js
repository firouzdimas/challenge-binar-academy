'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('customers', [{
      cust_name: 'Dimas Sadewo',
      email: 'Sadewo@mail.com',
      phone_num: 62812345678,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('customers', [{
      cust_name: 'Dimas Sadewo',
      email: 'Sadewo@mail.com',
      phone_num: 62812345678,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  }
};
