'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('products', [{
      product_name: 'katsu',
      price: 10000,
      createdAt: new Date(),
      updatedAt: new Date(),
      orderId: 1
    }], {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('products', [{
      product_name: 'kaatsu',
      price: 10000,
      createdAt: new Date(),
      updatedAt: new Date(),
      orderId: 1
    }], {});
  }
};
