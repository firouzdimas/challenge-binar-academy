function vocalSeeker(board) {
    // Write your code here
    let jumlah = 'aiueoAIUEO'
    let vokal = ''
    

for(i=0; i<board.length; i++){
  for(j=0; j<board[i].length; j++){
    for(k=0; k<jumlah.length; k++){
      if(board[i][j] === jumlah[k]){
        vokal += board[i][j]
      }
    }
  }
}
let panjang = vokal.length

return `Vokal ditemukan ${panjang} dan kumpulan vokal adalah ${vokal}`
}
//DRIVER CODE

let board = [
  ['*', '*', '*', 10],
  ['*', '*', -5, -10, '*', 100],
  ['a', 'A', 'o', 'b']
]

console.log(vocalSeeker(board)); // vokal ditemukan 3 dan kumpulan vokal adalah aAo