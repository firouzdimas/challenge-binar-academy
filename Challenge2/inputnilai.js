const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

const nilaiAngka = {
    name: '',
    nilai: []
}

function inputnilai(){
    readline.question('Tekan Enter lalu Input nilai siswa dan ketik \"q\" pada akhir input nilai', (answer) => {
        nilaiAngka.name = +answer;
        readline.setPrompt('Masukkan nilai: ')
        readline.prompt()
        readline.on('line', (kata) => {    
            if (kata !== 'q') {
                nilaiAngka.nilai.push(+kata)    
            }
            if (kata.toLowerCase().toString() == 'q') {
                readline.close() 
            } else {
                readline.setPrompt(`Masukkan nilai: `)
                readline.prompt()
            }
        })
    })
    readline.on('close', function() {

function urutannilai(arr){
    var len = arr.length;
    for (let i = len -1; i >= 0; i--) {
        for (let j = 1; j <= i; j++) {
            if (arr[j-1]>arr[j]) {
                var temp = arr[j-1];
                arr[j-1] = arr[j]
                arr[j] = temp
            }
        }
    }
    return 'Nilai terendah ke tertinggi: '+arr
}


function rataRata(arr) {
    var total = 0
    arr.forEach(function(score) {
        total += score
    });
    var avg = total/arr.length
    return `Rata-rata: ${Math.round(avg)}`;
}



function lulus(arr) {
    for (let i = 0; arr <= i; i++) {
      if (arr >= 75) {
        return arr;
      }
    }
    return arr >= 75;
}

function tidakLulus(arr) {
    for (let i = 0; arr <= i; i++) {
      if (arr < 75) {
        return arr;
      }
    }
    return arr < 75;
}

console.log(`Nilai tertinggi: ${Math.max(...nilaiAngka.nilai)}
Nilai terendah:  ${Math.min(...nilaiAngka.nilai)}
${urutannilai(nilaiAngka.nilai)}
${rataRata(nilaiAngka.nilai)}
Jumlah siswa lulus: ${nilaiAngka.nilai.filter(lulus).length}
Jumlah siswa tidak lulus: ${nilaiAngka.nilai.filter(tidakLulus).length}`)
        process.exit()
    }) 
}
inputnilai()
